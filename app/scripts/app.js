'use strict';

/**
 * @ngdoc overview
 * @name basicApp
 * @description
 * # basicApp
 *
 * Main module of the application.
 */
angular.module('basic', [
  'ngAnimate',
  'ngAria',
  'ngCookies',
  'ngMessages',
  'ngResource',
  'ui.router',
  'ngSanitize',
  'ngTouch',
  'pascalprecht.translate',
  'ngFileUpload',
  "isteven-multi-select",
  "dndLists",
  'ui.bootstrap',
  'ui-notification',
  'angularSpinner',
  'ngCookies',
  'ui.select',
  'toggle-switch',
  'cfp.hotkeys',
  'ui.bootstrap.datetimepicker',
  'angularMoment',
  'chart.js',
  'ui.router.state.events',
  'basic.router',
  'basic.resource',
  'basic.services',
  'basic.controller',
  'basic.filter',
  'treeControl',
  'highcharts-ng',
  'pageslide-directive',
  'basic.directive',
  'ngLodash'
]).constant('GLOBAL', {
  size: 10,
  host: './graphene-service-1.0-SNAPSHOT/v1/api',
  //sso_switch: 'true',
  sso_switch: '<SSO_SWITCH>',
}).constant('AUTH_EVENTS', {
  loginNeeded: 'auth-login-needed',
  loginSuccess: 'auth-login-success',
  httpForbidden: 'auth-http-forbidden'
}).constant('_',
  window._
).run(['$rootScope', function ($rootScope) {




$rootScope.dataForTheTree = [
  {
      name: '实时查询',
      img: 'icon25 uex-icon-data-query',
      // url: 'console.build@' + namespace,
      url:'console.real-time-query',
      stateUrl: null,
      children: []
  },
  {
      name: '离线查询',
      img: 'icon25 uex-icon-data-log-query',
      url: 'console.offline-query',
      stateUrl: null,
      children: []
  },


];

$rootScope.sidebarIsShow = true;
  $rootScope.$on('$stateChangeStart', function (event, toState,$stateParams) {
    $rootScope.tab = toState.name;
    if(toState.name == "console.create-offline-query" && $stateParams.task_id){
      $rootScope.tab = "console.update-offline-query"
    }
    if(toState.name.indexOf('tenant') != -1 ){
      $rootScope.sidebarIsShow = true;
    }else{
      $rootScope.sidebarIsShow = false;
    }
    // 实时查询
    if (toState.name.indexOf('real-time-query') !== -1) {
      $rootScope.dataForTheTree[0].stateUrl = toState.name;
    }
    // 离线查询
    if (toState.name.indexOf('offline-query') !== -1) {
      $rootScope.dataForTheTree[1].stateUrl = toState.name;
    }

    // 离线查询 / 作业详情
    if (toState.name.indexOf('console.offline-detail') !== -1) {
      $rootScope.dataForTheTree[1].stateUrl = toState.name;
    }


  });

}])

