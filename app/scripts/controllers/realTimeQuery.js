'use strict';
/**
 * Main Controller
 */
angular.module('basic')
  .controller('RealTimeQueryCtrl', ['$scope','realQuery','RealQueryModel', function ($scope,realQuery,RealQueryModel) {
    //$scope.nodes = realAllData;
    //console.log('realAllData', realAllData);
    $scope.modelName = RealQueryModel.getInterface();
    //console.log('RealQueryModel.getInterface()', RealQueryModel.getInterface());
    $scope.curModelName = $scope.modelName[0];
    $scope.changeModelName = function(name){
      $scope.curModelName = name;
      $scope.modeScope = RealQueryModel.getModelScope(name).modelScopeArr;
    }

    $scope.modeScope = RealQueryModel.getModelScope($scope.curModelName).modelScopeArr;
    //console.log('$scope.modeScope', $scope.modeScope);
    $scope.curModelScope = $scope.modeScope[0];
    $scope.changeModelScope = function(name){
      $scope.curModelScope = name;
    }
    $scope.searchTxt = '';
    $scope.search = function(){
      var urlObj = RealQueryModel.getModelScope($scope.curModelName).urlObj;
      realQuery.get({
        scope:$scope.curModelScope,
        //interface:'stepByProperty',
        interface:urlObj.interface,
        //property:'phonenum',
        property:urlObj.property,
        //label:'called',
        label:urlObj.label,
        //step:3,
        step:urlObj.step,
        value:$scope.searchTxt,
      }, function (data) {
      $scope.nodes = data
        $scope.$broadcast ('hasnode', $scope.nodes,$scope.curModelScope,urlObj.label,$scope.searchTxt);
        //console.log('data', data);
      })
    }
    $scope.widthHeight = {
        width : $('#sidebar-right-fixed').width(),
        height : document.body.clientHeight
    }
    //$scope.nodes = {
    //  "vertexes": [
    //    {
    //      "vid": 4256,
    //      "vlabel": "demigod",
    //      "properties": {
    //        "age": 30,
    //        "name": "hercules"
    //      }
    //    },
    //    {
    //      "vid": 4136,
    //      "vlabel": "god",
    //      "properties": {
    //        "age": 5000,
    //        "name": "jupiter"
    //      }
    //    },
    //    {
    //      "vid": 4232,
    //      "vlabel": "titan",
    //      "properties": {
    //        "age": 10000,
    //        "name": "saturn"
    //      }
    //    }
    //  ],
    //  "edges": [
    //    {
    //      "inV": 4136,
    //      "outV": 4256,
    //      "eid": "1zo-3a8-7x1-36w",
    //      "elabel": "father",
    //      "properties": {}
    //    },
    //    {
    //      "inV": 4232,
    //      "outV": 4136,
    //      "eid": "1z9-36w-7x1-39k",
    //      "elabel": "father",
    //      "properties": {}
    //    }
    //  ]
    //}





  }]);
