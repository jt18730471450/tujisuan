'use strict';
/**
 * Main Controller
 */
angular.module('basic')
  .controller('CreateOfflineQueryCtrl', ['$scope','TaskAdd','moment','taskDatail','$stateParams','$state','changTimes','TaskModel','$rootScope',
    function ($scope,TaskAdd,moment,taskDatail,$stateParams,$state,changTimes,TaskModel,$rootScope) {
      $scope.grid = {
        st: null
      }

      $scope.taskTypeArr = TaskModel.getModelName();
      $scope.ifPeriod = false; ///循环任务是否展示
      $scope.taskType = $scope.taskTypeArr[0];
      $scope.requestData = TaskModel.getTaskObj($scope.taskType);
      //////////执行时间分类
      $scope.periodArr = ['天','时','分'];
      /////////切换作业类型
      $scope.changeTaskType = function(name){
        $scope.taskType = name;
        $scope.requestData = TaskModel.getTaskObj($scope.taskType);
        if($scope.requestData.type == 2){
          $scope.ifPeriod = true;
        }
      }
      //////////执行时间分类时数字限制
      $scope.period = {};
      $scope.maxLenght = '365'
      $scope.changePd = function(pd){
        $scope.maxLenght = '0'
        $scope.period.cuePeriod = pd;
        if(pd == '天'){
          $scope.maxLenght = '365'
        }else if(pd == '时'){
          $scope.maxLenght = '24'
        }else if(pd == '分'){
          $scope.maxLenght = '60'
        }
      }
      $scope.period.cuePeriod = '天';
      $scope.period.pdNum = 0;
      $scope.errObj={
        requestErr : false,
        timeErr : false
      }
      ////编辑时获取task详情
      if($stateParams.task_id){
        var curId = $stateParams.task_id;
        taskDatail.get({task_id:curId}, function (data) {
          $scope.grid = {
            st: changTimes.getDateTime(data.schedule)
          }
          ///判断作业类型
          if(data.task_builder == 'cluster'){
            $scope.taskType = $scope.taskTypeArr[0];
          }else{
            $scope.taskType = $scope.taskTypeArr[1];
          }
          if(data.type == 1 && data.task_builder == 'cluster' || data.type == 2){
            $scope.ifPeriod = true;
          }
          //存在循环任务
          if(data.period){
             $scope.period.pdNum = parseInt(data.period)
             if(data.period.indexOf('d') != -1){
                $scope.period.cuePeriod == '天'
             }else if(data.period.indexOf('h') != -1){
              $scope.period.cuePeriod == '时'
            }else if(data.period.indexOf('m') != -1){
              $scope.period.cuePeriod == '分'
            }
          }
          $scope.requestData.type = data.type;
          $scope.requestData.inputObj = JSON.parse(data.request_data);
        });
      }
      $scope.createQuery = function(){
        var createQueryJson = {
          "type": '',
          "request_data": "{}",
          "schedule": "",
          "task_builder": "cluster",
          "period":"",
          "create_time":""
        }
        var curTime = new Date().getTime();
        createQueryJson.type = $scope.requestData.type;
        createQueryJson.create_time = moment(curTime).format('YYYY-MM-DD HH:mm:ss');
        if($scope.requestData.type == 0){
          createQueryJson.task_builder = 'cluster'
           if(!$scope.requestData.inputObj.inputpath || !$scope.requestData.inputObj.target_phones){
             $scope.errObj.requestErr = true;
             return;
           }else{
             $scope.errObj.requestErr = false;
           }
        }else if($scope.requestData.type == 2){
          createQueryJson.task_builder = 'bulkload'
          if(!$scope.requestData.inputObj.inputpath || !$scope.requestData.inputObj.outputpath){
            $scope.errObj.requestErr = true;
            return;
          }else{
            $scope.errObj.requestErr = false;
          }
        }
        if(!$scope.grid.st){
          $scope.errObj.timeErr = true;
          return;
        }else{
          $scope.errObj.timeErr = false;
        }
        createQueryJson.schedule = moment($scope.grid.st).format('YYYY-MM-DD HH:mm:ss');
        createQueryJson.schedule = changTimes.setDateTime(createQueryJson.schedule);

        createQueryJson.request_data = JSON.stringify($scope.requestData.inputObj);

        if($scope.ifPeriod){
          if($scope.requestData.type == 0){
            createQueryJson.type = 1;
          }else{
            createQueryJson.type = 2;
          }
          if($scope.period.cuePeriod == '天'){
            createQueryJson.period = $scope.period.pdNum+'d'
          }else if($scope.period.cuePeriod == '时'){
            createQueryJson.period = $scope.period.pdNum+'h'
          }else if($scope.period.cuePeriod == '分'){
            createQueryJson.period = $scope.period.pdNum+'m'
          }
        }
        console.log('createQueryJson',createQueryJson);
        TaskAdd.put({},createQueryJson, function (res) {
          $state.go('console.offline-query');
        }, function (err) {

        })
      }
    }]);
