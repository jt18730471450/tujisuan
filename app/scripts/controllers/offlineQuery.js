'use strict';
/**
 * Main Controller
 */

angular.module('basic')
  .controller('OfflineQueryCtrl', ['$rootScope', '$scope', 'alltask', 'Tasklist', 'deleteTaskDatail', 'GLOBAL','stopTaskDatail','allTask','Sort','$state',
    function ($rootScope, $scope, alltask, Tasklist, deleteTaskDatail, GLOBAL,stopTaskDatail,allTask,Sort,$state) {
      $scope.text = "您还没有离线列表数据";
      $scope.begin_blank = true;
      // 分页
      $scope.grid = {
        page: 1,
        size: GLOBAL.size,
        txt: '',
        changestatus: '所有任务',
        status: [{ name: '所有任务' }, { name: '简单任务' }, { name: '循环任务' }]
      };
      $scope.paixulist = null;
      // 侦听选择数据变化
      $scope.$watch('grid.changestatus', function (n, o) {
        if (n == o) {
          return;
        }
        if (n !== '所有任务') {
          if (!$scope.paixulist) {
            $scope.paixulist = angular.copy($scope.alldata);
            angular.forEach($scope.paixulist, function (item, i) {
              // 作业类型判断
              switch (item.type) {
                case 0:
                  item.stylename = '简单任务';
                  break;
                case 1 :
                  item.stylename = '循环任务';
                  break;
                case  2:
                  item.stylename = '循环任务';
                  break;
                default : item.stylename = '所有任务';
              }
            })
          }
          var newitems = []
          angular.forEach($scope.paixulist, function (item, i) {
            if (item.stylename == n) {
              newitems.push(item)

            }
          })
          $scope.alldata = angular.copy(newitems);
          $scope.grid.total = $scope.alldata.length;
          $scope.grid.page = 1;
          refresh(1)
        } else {
          $scope.alldata = angular.copy($scope.paixulist);
          $scope.grid.total = $scope.alldata.length;
          $scope.grid.page = 1;
          refresh(1)
        }

      });

      $scope.$watch('grid.page', function (newVal, oldVal) {
        if (newVal != oldVal) {
          refresh(newVal);
        }
      });

      var refresh = function (page) {
        $(document.body).animate({
          scrollTop: 0
        }, 200);
        var skip = (page - 1) * $scope.grid.size;
        $scope.itemdata = $scope.alldata.slice(skip, skip + $scope.grid.size);
        // console.log("123", $scope.itemdata)
      };
      //搜索任务
      $scope.offlinesearch = function (event) {
        $scope.grid.page = 1;
        if (!$scope.grid.txt) {
          $scope.alldata = angular.copy($scope.copydata);
          refresh(1);
          $scope.grid.total = $scope.alldata.length;
          return;
        }
        $scope.alldata = [];
        var iarr = [];
        var str = $scope.grid.txt;
        str = str.toLocaleLowerCase();
        angular.forEach($scope.copydata, function (item, i) {
          var nstr = item.task_id;
          nstr = nstr.toLocaleLowerCase();
          if (nstr.indexOf(str) !== -1) {
            iarr.push(item)
          }
        })

        if (iarr.length === 0) {
          $scope.begin_blank = false;
          $scope.text = '没有查询到符合条件的数据';
          // console.log($scope.items.length);
        }
        else {
          $scope.text = '您还没有离线列表数据';
        }
        $scope.alldata = angular.copy(iarr);
        refresh(1);
        $scope.grid.total = $scope.alldata.length;
      };
      // 删除列表数据
      $scope.deldata = function (id) {
        deleteTaskDatail.remove({
          task_id: id
        }, function (res) {
          for (var i = 0; i < $scope.alldata.length; i++) {
            if (id == $scope.alldata[i].task_id) {
              $scope.alldata.splice(i, 1)
            }
          }
          $scope.grid.page = $scope.alldata.length;
          refresh(1);
        })
      }
      /////循环任务停止
      $scope.stopTask = function(id){
        stopTaskDatail.stop({task_id:id}, function (data) {
          angular.forEach($scope.alldata, function (item, i) {
            if(item.task_id == id){
              item.status = 5;
            }
          })
          $scope.copydata = angular.copy($scope.alldata);
          refresh(1);
        })
      };
      // 离线查询列表
        $scope.alldata = Sort.sort(allTask, -1);;
        $scope.copydata = angular.copy($scope.alldata);
        $scope.grid.total = $scope.alldata.length;
        $scope.grid.page = 1;
        $scope.grid.txt = '';
        refresh(1);
      $scope.addOrUpdate = function(id){
        $state.go('console.create-offline-query',{task_id: id})
      }

    }]);
