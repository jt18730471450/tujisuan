'use strict';
/**
 * Main Controller
 */
angular.module('basic')
  .controller('ConsoleCtrl', ['$rootScope', '$scope', '$window', '$state',
    function ($rootScope, $scope, $window, $state) {
      // 返回
      $scope.back = function () {
        $window.history.back();
      }
      // 不需要面包屑页面排除
      $scope.hasBack = function () {
        if ($state.current.name == "console.real-time-query" || $state.current.name == "console.offline-query") {
          return false
        }
        return true;
      };




    }]);
