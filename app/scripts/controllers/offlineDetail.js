'use strict';
/**
 * Main Controller
 */
angular.module('basic')
  .controller('OfflineDetailCtrl', ['$scope', '$state','TaskDatail', 'deleteTaskDatail','tasklog','$stateParams',
    function ($scope, $state,TaskDatail, deleteTaskDatail,tasklog,$stateParams) {
      //console.log('TaskDate', TaskDate);
      if (TaskDatail.type !== 1) {
        tasklog.query({id:$stateParams.task_id}, function (data) {

        })
      }else {

      }

      $scope.taskDatailItem = angular.copy(TaskDatail);
      let requestData = JSON.parse($scope.taskDatailItem.request_data);
      $scope.resultRequestData = [];
      Object.entries(requestData).forEach(function([k,v]){
        $scope.resultRequestData.push({'key':k,"value":v})
      });
      // console.log('$scope.taskDatailItem',result);
      $scope.deleteTaskDetail = function () {
        console.log('delete--starting-');
        deleteTaskDatail.remove({
          task_id: $scope.taskDatailItem.task_id
        },function (res) {
          console.log('DeleteTaskDatail ok',res);
          $state.go('console.offline-query');
        })
      };
      $scope.liIsSelected = [true,false];
      $scope.activeChange = function (id) {
        for(var  i = 0 ; i < $scope.liIsSelected.length; i++){
          $scope.liIsSelected[i] = false;
          if(id == i){
            $scope.liIsSelected[i] = true;
          }
        }
      };

      $scope.widthHeight = {
        width : $('#sidebar-right-fixed').width(),
        height : document.body.clientHeight
      };

      $scope.nodes = {
        "vertexes": [
          {
            "vid": 4256,
            "vlabel": "demigod",
            "properties": {
              "age": 30,
              "name": "hercules"
            }
          },
          {
            "vid": 4136,
            "vlabel": "god",
            "properties": {
              "age": 5000,
              "name": "jupiter"
            }
          },
          {
            "vid": 4232,
            "vlabel": "titan",
            "properties": {
              "age": 10000,
              "name": "saturn"
            }
          }
        ],
        "edges": [
          {
            "inV": 4136,
            "outV": 4256,
            "eid": "1zo-3a8-7x1-36w",
            "elabel": "father",
            "properties": {}
          },
          {
            "inV": 4232,
            "outV": 4136,
            "eid": "1z9-36w-7x1-39k",
            "elabel": "father",
            "properties": {}
          }
        ]
      }
    }]);
