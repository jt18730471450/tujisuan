"use strict";
angular.module('basic.filter', [])
  .filter('lowerCase', [function () {
    return function (res) {
      //console.log('res', res);
      if (res) {
        return res.toLowerCase();
      }

    };
  }])

  // 面包屑设置
  .filter('stateNameFilter', [function () {
    return function (state) {
      switch (state) {
        case "console.create-offline-query":
          return "离线查询"
        case "console.update-offline-query":
          return "离线查询"
        case "console.offline-detail":
          return "离线查询"

      }
    };

  }])

  .filter('stateTitleFilter', [function () {
    return function (state) {
      switch (state) {
        case "console.create-offline-query":
          return "新建作业"
        case "console.update-offline-query":
          return "编辑作业"
        case "console.offline-detail":
          return "作业详情"

      }
    };

  }])
  // 运行状态
  .filter('phaseFilter', [function () {
    return function (status) {
      if (status == 0) {
        return "准备"
      } else if (status == 1) {
        return "运行中"
      } else if (status == 2) {
        return "完成"
      } else if (status == 3) {
        return "失败"
      } else if (status == 4) {
        return "正常"
      } else if (status == 5) {
        return "停止"
      } else {
        return phase || "-"
      }
    };
  }])

  // 作业类型
  .filter('typeFilter', [function () {
    return function (type) {
      if (type == 0) {
        return "简单任务"
      } else if (type == 1 || type == 2) {
        return "循环任务"
      }else {
        return type || "-"
      }
    };
  }])
  .filter('scheduleTime', [function () {
    return function (time) {
      if(time){
        let timeArr = time.split(' ')
        timeArr[0] = timeArr[0].replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");
        return timeArr.join(' ');
      }
    };
  }])
