/**
 * Created by sorcerer on 2017/6/7.
 */
"use strict";
angular.module('basic.services', ['ngResource', 'ui.bootstrap', 'ui.bootstrap.datetimepicker'])
  .service('Cookie', [function () {
    this.set = function (key, val, expires) {
      let date = new Date();
      date.setTime(date.getTime() + expires);
      document.cookie = key + "=" + val + "; expires=" + date.toUTCString();
    };
    this.get = function (key) {
      let reg = new RegExp("(^| )" + key + "=([^;]*)(;|$)");
      let arr = document.cookie.match(reg);
      if (arr) {
        return (arr[2]);
      }
      return null;
    };
    this.clear = function (key) {
      this.set(key, "", -1);
    };
  }])

  /*
   * This file is mainly for modal dialog popup window, return $uibModal.open().result promise for next operation
   * TODO: merge all the services into one, use functions to do route.
   */

  .service('newconfirm', ['$uibModal', function ($uibModal) {
    this.open = function (datacon, status) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/newconfirm.html',
        size: 'default',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.con = datacon;
          $scope.con = {};
          for (let key in datacon) {
            if (datacon[key]) {
              $scope.con[key] = datacon[key];
            }
          }
          if (status) {
            $scope.status = status;
          }
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
          $scope.ok = function () {
            $uibModalInstance.close(true);
          };
        }]
      }).result;
    };
  }])
  .service('infoconfirm', ['$uibModal', function ($uibModal) {
    this.open = function (datacon) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/infoconfirm.html',
        size: 'default',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.con = datacon;
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
          $scope.ok = function () {
            $uibModalInstance.close(true);
          };
        }]
      }).result;
    };
  }])
  .service('delconfirm', ['$uibModal', function ($uibModal) {
    this.open = function (title, roleId, userId, username) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/delConfirm.html',
        size: 'default',
        controller: ['$scope', '$uibModalInstance', 'deltenantuser', function ($scope, $uibModalInstance, deltenantuser) {
          $scope.title = title;
          $scope.userId = userId;
          $scope.username = username;
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
            $scope.delfail = false;
          };
          $scope.delfail = false;
          $scope.ok = function () {
            deltenantuser.delete({id: roleId, userId: userId}, {}, function (res) {
              $uibModalInstance.close(res);
            }, function () {
              $scope.delfail = true;
            });
          };
        }]
      }).result;
    };
  }])
  .service('Alert', ['$uibModal', function ($uibModal) {
    this.open = function (con) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/Alert.html',
        size: 'default',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.con = con;
            $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
          $scope.ok = function () {
            $uibModalInstance.dismiss();
          };
        }]
      }).result;
    };
  }])
  .service('user_change_Confirm', ['$uibModal', function ($uibModal) {
    this.open = function (item) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/user_change_Confirm.html',
        size: 'default',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.username = item.username;
          $scope.email = item.email;
          $scope.description = item.description;
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
          $scope.ok = function () {
            $uibModalInstance.close(true);
          };
        }]
      }).result;
    };
  }])
  .service('smallAlert', ['$uibModal', function ($uibModal) {
    this.open = function (con) {
      return $uibModal.open({
        backdrop: 'static',
        templateUrl: 'views/tpl/small_alert.html',
        size: 'default small_alert',
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.con = con;
          let closeConf = function () {
            $uibModalInstance.close();
          };
          window.setTimeout(closeConf, 1500);
        }]
      }).result;
    };
  }])
  .factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', 'Cookie',
    function ($rootScope, $q, AUTH_EVENTS, Cookie) {
      let CODE_MAPPING = {
        401: AUTH_EVENTS.loginNeeded,
        403: AUTH_EVENTS.httpForbidden,
        419: AUTH_EVENTS.loginNeeded,
        440: AUTH_EVENTS.loginNeeded
      };
      return {
        request: function (config) {
          if (/^\/login/.test(config.url)) {
            return config;
          }
          let tenantId = Cookie.get("tenantId");
          let username = Cookie.get("username");
          if (config.headers) {
            config.headers.tenantId = tenantId;
            config.headers.username = username;
          }
          if (config.headers) {
            config.headers.http_x_proxy_cas_loginname = "like";
            config.headers.http_x_proxy_cas_username = "like";
          }
          if (config.headers) {
            config.headers.token = Cookie.get('token');
          }
          $rootScope.loading = true;
          return config;
        },
        requestError: function (rejection) {
          $rootScope.loading = false;
          return $q.reject(rejection);
        },
        response: function (res) {
          $rootScope.loading = false;
          return res;
        },
        responseError: function (response) {
          $rootScope.loading = false;
          let val = CODE_MAPPING[response.status];
          if (val) {
            $rootScope.$broadcast(val, response);
          }
          return $q.reject(response);
        }
      };
    }])
  .factory('RealQueryModel', [
    function () {
      var realArr = [{
        "model_name": "OneStepQuery",
        "model_scope": ["test"],
        "model_params": [{
          "param_name": "interface",
          "param_value": "stepByProperty",
          "param_type": "constant"
        }, {
          "param_name": "property",
          "param_value": "phonenum",
          "param_type": "constant"
        }, {
          "param_name": "value",
          "param_value": "",
          "param_type": "variable"
        }, {
          "param_name": "label",
          "param_value": "called",
          "param_type": "constant"
        }, {
          "param_name": "step",
          "param_value": "1",
          "param_type": "constant"
        }],
        "display_field": {
          "property_name": "phonenum",
          "length": 8
        }
      }, {
        "model_name": "TwoStepQuery",
        "model_scope": ["graph1", "graph2"],
        "model_params": [{
          "param_name": "interface",
          "param_value": "stepByProperty",
          "param_type": "constant"
        }, {
          "param_name": "property",
          "param_value": "phonenum",
          "param_type": "constant"
        }, {
          "param_name": "value",
          "param_value": "",
          "param_type": "variable"
        }, {
          "param_name": "label",
          "param_value": "called",
          "param_type": "constant"
        }, {
          "param_name": "step",
          "param_value": "2",
          "param_type": "constant"
        }],
        "display_field": {
          "property_name": "phonenum",
          "length": 8
        }
      }];
      return {
          getInterface:function(){
             var interfaceArr = [];
             for(var i = 0 ; i < realArr.length; i++){
               interfaceArr.push(realArr[i].model_name);
             }
             return interfaceArr;
          },
          getModelScope : function(name){
              var modelScopeObj = {
                modelScopeArr : [],
                urlObj:{
                  interface:'',
                  property:'',
                  label:'',
                  value:'',
                  step:'',

                }
              };
              for(var  i = 0 ; i < realArr.length; i++){
                 if(name == realArr[i].model_name){
                   modelScopeObj.modelScopeArr = realArr[i].model_scope;
                   angular.forEach(realArr[i].model_params, function (model,i) {
                        if(model.param_name == "interface"){
                          modelScopeObj.urlObj.interface = model.param_value;
                        }else if(model.param_name == "property"){
                          modelScopeObj.urlObj.property = model.param_value;
                        }else if(model.param_name == "label"){
                          modelScopeObj.urlObj.label = model.param_value;
                        }else if(model.param_name == "step"){
                          modelScopeObj.urlObj.step = model.param_value;
                        }

                   })
                 }
              }
            return modelScopeObj;
          }

      };
    }])
  .service('TaskModel', [
    function () {
      let taskModel= [
        {
          "model_name":"Clustering",
          "task_params":[
            {
              "param_name":"task_builder",
              "param_value":"cluster",
              "param_type":"constant"
            },
            {
              "param_name":"inputpath",
            "param_value":"",
              "param_type":"variable"
            },
            {
              "param_name":"target_phones",
              "param_value":"",
              "param_type":"variable"
            }
            ]
         },
      {
        "model_name":"Bulkload",
        "task_params":[
        {
          "param_name":"task_builder",
          "param_value":"bulkload",
          "param_type":"constant"
        },
        {
          "param_name":"inputpath",
        "param_value":"",
        "param_type":"variable"
        },
        {
          "param_name":"outputpath",
          "param_value":"",
          "param_type":"variable"
        }
        ],
      }
      ]
      return {
        getModelName:function(){
          var modelArr = [];
          for(var i = 0 ; i < taskModel.length; i++){
            modelArr.push(taskModel[i].model_name);
          }
          return modelArr;
        },
        getTaskObj:function(name){
            let taskObj  = {
              task_builder:'',
              inputObj:{
                inputpath:'',
              },
              type:null
            };
          if(name == 'Clustering'){
            taskObj.inputObj.target_phones = '';
            taskObj.type = 0;
          }else{
            taskObj.inputObj.outputpath = '';
            taskObj.type = 2;
          }
          return taskObj;
        }
      }
  }])
  .service('changTimes', [function () {

    this.setDateTime = function (time) {
        if(time){
          return time.split('-').join('');
        }
    };
    this.getDateTime = function (time) {
      if(time){
        let timeArr = time.split(' ')
        timeArr[0] = timeArr[0].replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");
        return timeArr.join(' ');
      }
    };
  }])
  .service('Sort', [function() {
    this.sort = function(items, reverse) {
      if (!reverse || reverse == 0) {
        reverse = 1;
      }
      items.sort(function(a, b) {
        if (!a.create_time) {
          return 0;
        }
        return reverse * ((new Date(a.create_time)).getTime() - (new Date(b.create_time)).getTime());
      });
      return items;
    };
  }])


