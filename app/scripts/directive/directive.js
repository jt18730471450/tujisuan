/**
 * Created by sorcerer on 2018/7/2.
 */
"use strict";
angular
  .module("basic.directive", [])
  //头部自定义
  .directive('cHeader', [function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'components/header/header.html',
      controller: ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

      }]
    }
  }])
  .directive('cCheckbox', [function () {
    return {
      restrict: 'EA',
      replace: true,
      scope: {
        checked: '=',
        text: '@'
      },
      templateUrl: 'views/tpl/directives/checkbox.html'
    }
  }])
  .directive('datePick', [function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'views/tpl/directives/datepick.html',
      scope: {
        st: '=',
        et: '='
      },
      controller: ['$scope', function ($scope) {
        $scope.datePickerOptions = {
          showWeeks: false,
          formatDay: 'd',
          minDate: new Date(),
          formatDayTitle: 'yyyy年M月',
          customClass: function (date, mode) {
            if (!$scope.st || !$scope.et || !date) {
              return;
            }
            if (date.mode != 'day') {
              return;
            }
            if ($scope.st.getMonth() == date.date.getMonth() && $scope.st.getDate() == date.date.getDate()) {
              return "day-end"
            }
          }
        };

        $scope.$watch('dt', function (newVal, oldVal) {
          if (newVal == oldVal || !newVal) {
            return;
          }
          if (!oldVal) {
            $scope.st = newVal;
          } else {
            $scope.dt = null;
            $scope.opened = false;
          }
        });
      }]
    };
  }])
  // 左导航自定义
  .directive('cSidebar', [function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'components/sidebar/sidebar.html',
      controller: ['$state', '$scope', '$rootScope',
        function ($state, $scope, $rootScope) {
          $('#sidebar-container').css('overflow-y', 'auto');
          $scope.mouHoverOne = function (that) {
            $(that.currentTarget).children('ul').show();
          }
          $scope.mouHoverTwo = function (that) {
            $(that.currentTarget).children('ul').hide();
          }
          $scope.nodeUrlGo = function (url) {
            if (url) {
              var urlarr = url.split('@');
              $state.go(urlarr[0], ({namespace: urlarr[1]}));
            }
          }
          $scope.state = $state;
          $scope.goUrl = function (url) {
            if (url) {
              var urlarr = url.split('@');
              if (urlarr && urlarr.length) {
                $state.go(urlarr[0], ({namespace: urlarr[1]}));
              }
            } else {
              $scope.activeStyle = false;
              //  alert(2);
              $(".zx_set_btn").removeClass("zx_set_btn_rotate");
              $("#sidebar-container").removeClass("sider_zx");
              $("#sidebar-right-fixed").removeClass("sidebar-fixed");
            }


          };

          $scope.activeStyle = false;
          $scope.treeOptions = {
            nodeChildren: "children",
            dirSelectable: false,
            injectClasses: {
              ul: "a1",
              li: "a2",
              liSelected: "a7",
              iExpanded: "a3",
              iCollapsed: "a4",
              iLeaf: "a5",
              label: "a6",
              labelSelected: "a8"
            }
          };
          var width = 0;
          width = $(window).width() - 168;
          $scope.sidebaerWidth = function () {
            //alert(3)
            $(".zx_set_btn").toggleClass("zx_set_btn_rotate");
            $("#sidebar-container").toggleClass("sider_zx");
            $("#sidebar-right-fixed").toggleClass("sidebar-fixed");

            $(".bread_set").toggleClass("bread_set_toggle")
            $(".sb-arrow").toggleClass("rotate");
            if ($("#sidebar-container").hasClass("sider_zx")) {
              $(".nav_top_li").addClass("nav_top_toggle");
              $(".bread_set").addClass("bread_set_toggle")

            } else {
              $(".nav_top_li").removeClass("nav_top_toggle");
              $(".bread_set").removeClass("bread_set_toggle")
            }
            if ($(".zx_set_btn").hasClass('zx_set_btn_rotate')) {
              $scope.activeStyle = true;
              $('#sidebar-container').css('overflow-y', 'visible')

            } else {
              $scope.activeStyle = false;
              // alert(4);
              $('#sidebar-container').css('overflow-y', 'auto')
            }
          };

        }]
    }
  }])
  // 输入框头部
  .directive('addClassesTop', [function () {
    return function (scope, element, attr) {
      if ($("#sidebar-container").hasClass("sider_zx")) {
        element.addClass("nav_top_toggle");
      } else {
        element.removeClass("nav_top_toggle");
      }

    }
  }])
  // 面包屑头部
  .directive('addClassesMap', [function () {
    return function (scope, element, attr) {
      if ($("#sidebar-right-fixed").hasClass("sidebar-fixed")) {
        element.addClass("bread_set_toggle");
      } else {
        element.removeClass("bread_set_toggle");
      }

    }
  }])

  .directive("fullHeight", [
    function () {
      return function (scope, element, attr) {
        var height = document.documentElement.clientHeight - 70 + "px";
        element.css({
          "min-height": height
        });
      };
    }
  ])
  .directive("confullHeight", [
    function () {
      return function (scope, element, attr) {
        var height = document.documentElement.clientHeight - 150 + "px";
        element.css({
          height: height,
          // 'position':'relative',
          overflow: "auto"
        });
      };
    }
  ])
  .directive('line', function () {
    return {
      scope: {
        id: "@",
        nodes: "=",
        link: "="
      },
      restrict: 'E',
      template: '<div style="height:600px;width:800px;"></div>',
      replace: true,
      link: function ($scope, element, attrs, controller) {
        var nodelist = angular.copy($scope.nodes.vertexes)
        var edgeslist = angular.copy($scope.nodes.edges)
        var clickid = [];
        var sizemap = {};
        var nodemap = {};
        var indexmap = {}
        var defaultsize = 30;
        var newnodes = {
          "vertexes": [
            {
              "vid": 1,
              "vlabel": "jiangtong",
              "properties": {
                "age": 1,
                "name": "jiangtong"
              }
            }, {
              "vid": 2,
              "vlabel": "jingxy",
              "properties": {
                "age": 11,
                "name": "jingxy"
              }
            },
          ],
          "edges": [{
            "inV": 4232,
            "outV": 1,
            "eid": "3",
            "elabel": "father",
            "properties": {}
          },
            {
              "inV": 4232,
              "outV": 2,
              "eid": "4",
              "elabel": "father",
              "properties": {}
            },
          ]
        }
        var data = [];
        var links = [];

        var categories = [];
        for (var i = 0; i < 9; i++) {
          categories[i] = {
            name: '类目' + i
          };
        }
        var colorarr = [
          {r: 194, g: 53, b: 49},
          {r: 47, g: 69, b: 84},
          {r: 97, g: 160, b: 168},
          {r: 212, g: 130, b: 101},
          {r: 145, g: 199, b: 174},
          {r: 116, g: 159, b: 131},
          {r: 202, g: 134, b: 34},
          {r: 189, g: 162, b: 154},
          {r: 110, g: 112, b: 116}
        ]

        function RandomNum(Min, Max) {
          var Range = Max - Min;
          var Rand = Math.random();
          var num = Min + Math.round(Rand * Range);
          return num;
        }

        function addnodelist(node) {
          var rep = false
          angular.forEach(nodelist, function (list) {
            if (list.vid === node.vid) {
              rep = true
            }
          })

          if (rep === false) {
            nodelist.push(node)
          }
        }

        function addedgeslist(link) {
          var rep = false
          angular.forEach(edgeslist, function (list) {
            if (list.eid === link.eid) {
              rep = true
            }
          })

          if (rep === false) {
            edgeslist.push(link)
          }
        }

        function creatsizemap(list) {
          sizemap = {}
          angular.forEach(list, function (item) {
            if (!sizemap[item.inV]) {
              sizemap[item.inV] = {
                size: 1
              }
            } else {
              sizemap[item.inV].size += 1
            }
            if (!sizemap[item.outV]) {
              sizemap[item.outV] = {
                size: 1
              }
            } else {
              sizemap[item.outV].size += 1
            }

          })
        }

        function creatimage(nodes) {
          angular.forEach(nodes.vertexes, function (node) {
            //addnodelist(node)


            node.itemStyle = {
              color: '#4788FB'
              //color: "rgb(" + colorarr[1]['r'] + "," + colorarr[1]['g'] + "," + colorarr[1]['b'] + ")"
            };
            //node.values = node["vid"];
            node.name = node["vlabel"];
//        console.log('node.name', node.name);
//            node.symbol = 'circle';
//            node.symbolSize = defaultsize
//        node.symbolSize /= 1.5;
            node.x = RandomNum(-350, 350)
            node.y = RandomNum(-250, 250)


            node.label = {
              normal: {
                show: node.symbolSize > 20
              }
            };
            node.category = '类目' + 1;
            data.push([node.x, node.y])
            node.index = data.length - 1;

            nodemap[node.vid] = node
            indexmap[node.index] = node
            //console.log('nodemap', nodemap);
          })
          //console.log('nodes.edges', nodes.edges);
          angular.forEach(nodes.edges, function (link, i) {
            console.log('link', link);
            links.push({})
            angular.forEach(nodemap, function (node) {
              if (link['inV'] === node.vid) {
                links[i].source = node.index;
              }
              if (link['outV'] === node.vid) {
                links[i].target = node.index;
              }
            })
            links[i].id = link.eid;
            links[i].elabel = link.elabel;
            links[i].properties = link.properties;
            links[i].label = {
              show: true,
              formatter: function (params) {
                return params.data['elabel']
              }
            };
          })

          //creatsizemap(edgeslist)
          //rewritesize(nodes.vertexes)
          //console.log('nodes.vertexes', data);
        }


        creatimage($scope.nodes)
        console.log('data', data);
        console.log('links', links);

        function rewritesize(nodes) {
          angular.forEach(nodes, function (node) {
            node.symbolSize = defaultsize + sizemap[node.vid].size * 5
          })

        }

        var myChart = echarts.init(document.getElementById("main"));

        //console.log('$scope.nodes', $scope.nodes);
        var option = {
          title: {
            text: '可拖动',
            //            subtext: 'Default layout',
            //            top: 'bottom',
            //            left: 'right'
          },
          tooltip: {
            formatter: function (params) {
              //console.log('params', params);
              if (params.dataType === 'node') {
                //console.log('nodemap[params.dataIndex]', indexmap[params.dataIndex]);
                return indexmap[params.dataIndex].name + '<br />' + '姓名：'
                  + indexmap[params.dataIndex].properties.name + '  年龄：'
                  + indexmap[params.dataIndex].properties.age
              } else {
                return indexmap[params.data.source].name + ' > ' + indexmap[params.data.target].name
              }

            }
          },
          animationDuration: 1500,
          animationEasingUpdate: 'quinticInOut',
          xAxis: {
            min: -500,
            max: 500,
            type: 'value',
            axisLine: {onZero: false},
            show: false
          },
          //同上
          yAxis: {
            min: -500,
            max: 500,
            type: 'value',
            axisLine: {onZero: false},
            show: false
          },
          series: [
            {
              id: 'a',
              type: 'graph',
              coordinateSystem: 'cartesian2d',
              symbolSize: 30,
              layout: 'none',
              data: data,
              links: links,
              categories: categories,
              edgeSymbol: ['none', 'arrow'],
              roam: true,
              label: {
                show: true,
                formatter: function (params) {
                  return indexmap[params.dataIndex].name
                }
              },
              //tooltip:{
              //  formatter: function (params) {
              //    if (params.dataType === "edge") {
              //       return indexmap[params.data.source].name+' > '+indexmap[params.data.target].name
              //    }
              //  }
              //},
              focusNodeAdjacency: true,
              itemStyle: {
                normal: {
                  color: '#4788FB',
                  borderColor: '#fff',
                  borderWidth: 1,
                  shadowBlur: 10,
                  shadowColor: 'rgba(0, 0, 0, 0.3)'
                }
              },
              lineStyle: {
                color: '#475669',
                //curveness: 0.3
              },
              emphasis: {
                lineStyle: {
                  width: 10
                }
              }
            }
          ]
        };

        //myChart.setOption(option);

        setTimeout(function () {
          //     // Add shadow circles (which is not visible) to enable drag.
          myChart.setOption({
            graphic: echarts.util.map(data, function (item, dataIndex) {
              return {
                type: 'circle',
                position: myChart.convertToPixel('grid', item),
                shape: {
                  cx: 0,
                  cy: 0,
                  r: 30 / 2
                },
                invisible: true,
                draggable: true,
                ondrag: echarts.util.curry(onPointDragging, dataIndex),
                onclick: echarts.util.curry(onPointonclick, dataIndex),
                //onmousemove: echarts.util.curry(showTooltip, dataIndex),
                //onmouseout: echarts.util.curry(hideTooltip, dataIndex),
                z: 100
              };
            })
          });
        }, 0);


        //下面都是死的，直接粘贴就行
        function showTooltip(dataIndex) {
          myChart.dispatchAction({
            type: 'showTip',
            seriesIndex: 0,
            dataIndex: dataIndex

          });
        }

        function hideTooltip(dataIndex) {
          myChart.dispatchAction({
            type: 'hideTip'
          });
        }

        function onPointDragging(dataIndex, dx, dy) {
          data[dataIndex] = myChart.convertFromPixel('grid', this.position);

          // Update data
          myChart.setOption({
            series: [{
              id: 'a',
              data: data
            }]
          });
        }

        function onPointonclick(dataIndex, dx, dy) {
          console.log('dataIndex', dataIndex);
        }

        //myChart.on('click', function (params) {
        //  console.log('params', params);
        //  //var clicked = false
        //  //angular.forEach(clickid, function (click) {
        //  //  if (click === params.data.vid) {
        //  //    clicked = true
        //  //  }
        //  //})
        //  //if (clicked) {
        //  //  return
        //  //}
        //  //clickid.push(params.data.vid)
        //  //if (params.data.vid === 4232) {
        //  //  creatimage(newnodes)
        //  //  $scope.nodes.vertexes = $scope.nodes.vertexes.concat(newnodes.vertexes)
        //  //  $scope.nodes.edges = $scope.nodes.edges.concat(newnodes.edges)
        //  //  rewritesize($scope.nodes.vertexes)
        //  //
        //  //  option.series[0].data = $scope.nodes.vertexes
        //  //  option.series[0].links = $scope.nodes.edges
        //  //  myChart.setOption(option, {
        //  //    notMerge: false,
        //  //    lazyUpdate: false,
        //  //    silent: false
        //  //  });
        //  //}
        //});
        if (option && typeof option === "object") {
          myChart.setOption(option, true);
        }
        //myChart.setOption(option);
      }
    }
  })
  .directive('graphChar', ['realQuery',function (realQuery) {
    return {
      scope: {
        id: "@",
        nodes: "=",
        link: "=",
        widthHeight: "=",
      },
      restrict: 'E',
      template: '<div style="height:600px;width:{{widthHeight.width}}"></div>',
      replace: true,
      link: function ($scope, element, attrs, controller) {
        // console.log('$scope.id', $scope.id);
        // console.log('$scope.nodes', $scope.nodes);

        var colorarr = [
          '#77C3F7',
          '#777FF7',
          '#F49B68',
          '#F66D7E',
          '#17DBA5',
          '#757CC7',
          '#79BBD9',
          '#757CC7',
          '#EFAB4A',
          '#8394AB'
        ];
        //$scope.nodes.edges = $scope.nodes.edges.slice(0, 500)
        var clickid = [];
        var sizemap = {};
        var defaultsize = 30;
        var newnodes = {
          "vertexes": [
            {
              "vid": 1,
              "vlabel": "jiangtong",
              "properties": {
                "age": 1,
                "name": "jiangtong"
              }
            }, {
              "vid": 2,
              "vlabel": "jingxy",
              "properties": {
                "age": 11,
                "name": "jingxy"
              }
            },
          ],
          "edges": [{
            "inV": 4232,
            "outV": 1,
            "eid": "3",
            "elabel": "father",
            "properties": {}
          },
            {
              "inV": 4232,
              "outV": 2,
              "eid": "4",
              "elabel": "father",
              "properties": {}
            },
          ]
        };
        var nodelist = '';
        var edgeslist = '';
        var option='';
        var categories = [];
        var myChart = echarts.init(document.getElementById($scope.id));
        for (var i = 0; i < 9; i++) {
          categories[i] = {
            name: '类目' + i
          };
        }
        function unique(arr){

          var res = [arr[0]];

          for(var i=1;i<arr.length;i++){

            var repeat = false;

            for(var j=0;j<res.length;j++){

              if(arr[i].vid == res[j].vid){

                repeat = true;

                break;

              }

            }

            if(!repeat){

              res.push(arr[i]);

            }

          }

          return res;

        }
        function RandomNum(Min, Max, zhou) {
          var Range = Max - Min;
          var Rand = Math.random();
          var num = Min + Math.round(Rand * Range);
          //console.log('nodelist', nodelist);

          var rep = false
          angular.forEach($scope.nodes.vertexes, function (node, i) {
            if (node[zhou] < num + 80 || node[zhou] > num - 80) {
              rep = true
              //console.log(node[zhou]);
            }
          })
          if (rep) {
            if (num + 80 < Max) {
              return num + 80
            } else if (num - 80 > Min) {
              return num - 80
            }else {
              return num
            }
          } else {
            return num;
          }

        }

        function addnodelist(node) {
          var rep = false
          angular.forEach(nodelist, function (list) {
            if (list.vid === node.vid) {
              rep = true
            }
          });

          if (rep === false) {
            nodelist.push(node)
          }
        }

        function addedgeslist(link) {
          var rep = false
          angular.forEach(edgeslist, function (list) {
            if (list.eid === link.eid) {
              rep = true
            }
          });

          if (rep === false) {
            edgeslist.push(link)
          }
        }

        function creatsizemap(list) {
          sizemap = {};
          angular.forEach(list, function (item) {
            if (!sizemap[item.inV]) {
              sizemap[item.inV] = {
                size: 1
              }
            } else {
              sizemap[item.inV].size += 1
            }
            if (!sizemap[item.outV]) {
              sizemap[item.outV] = {
                size: 1
              }
            } else {
              sizemap[item.outV].size += 1
            }

          })
        }

        function creatimage(nodes,id) {
          angular.forEach(nodes.vertexes, function (node) {
            //console.log(id,node['properties']['phonenum']);
            if (id && id === node['properties']['phonenum']) {
              console.log('id', id);
              node.itemStyle = {
                color: '#47B2FB'
                //color: "rgb(" + colorarr[1]['r'] + "," + colorarr[1]['g'] + "," + colorarr[1]['b'] + ")"
              };
            }else {
              node.itemStyle = {
                color: '#4788FB'
                //color: "rgb(" + colorarr[1]['r'] + "," + colorarr[1]['g'] + "," + colorarr[1]['b'] + ")"
              };
            }

            //node.values = node["vid"];
            //node.values = node["vid"];
            node.name = node["vid"].toString();
            //node.value = node["vid"].toString();
//        console.log('node.name', node.name);
            node.symbol = 'circle';
            node.symbolSize = defaultsize;
//        node.symbolSize /= 1.5;
            node.x = RandomNum(0, 1366);
            node.y = RandomNum(0, 768);

            node.label = {
              normal: {
                show: node.symbolSize > 20,
                formatter: function (params) {
                  //console.log('params', params);
                  //return params.data['vlabel']
                  return params.data['properties']['phonenum']
                }
              }
            };
            node.category = '类目' + 1;
            addnodelist(node)
          });

          angular.forEach(nodes.edges, function (link) {
            addedgeslist(link)
            link.target = link['inV'].toString();
            link.source= link['outV'].toString();
            angular.forEach(nodelist, function (node) {
              if (link['inV'] === node.vid) {
                link.targetname = node['properties']['phonenum'];
              }
              if (link['outV'] === node.vid) {
                link.sourcename = node['properties']['phonenum'];
              }
            });
            //link.label = {
            //  show: true,
            //  formatter: function (params) {
            //    return params.data['elabel']
            //  }
            //};


          });
          creatsizemap(edgeslist);
          rewritesize(nodes.vertexes);
          //console.log('nodes.vertexes', nodes.vertexes);
        }

        //creatimage($scope.nodes);
        //console.log('$scope.nodes', $scope.nodes);
        function rewritesize(nodes) {
          angular.forEach(nodes, function (node) {
            node.symbolSize = defaultsize + sizemap[node.vid].size * 1
          })

        }
        // console.log('myChart', myChart);
        //var nodelist = angular.copy($scope.nodes.vertexes);
        //var edgeslist = angular.copy($scope.nodes.edges);
        $scope.$on ('hasnode', function(e, nodes,curModelScope,lable,id) {
          $scope.nodes = nodes
          $scope.lable =lable
          $scope.curModelScope = curModelScope
          //console.log('hasnode', nodes);
          nodelist = angular.copy($scope.nodes.vertexes);
          edgeslist = angular.copy($scope.nodes.edges);
          creatimage($scope.nodes,id);
          option = {
          title: {
            // text: '不可拖动',
            //            subtext: 'Default layout',
            //            top: 'bottom',
            //            left: 'right'
          },
          tooltip: {
            formatter: function (params) {
              //console.log('params', params);
              if (params.data.vid) {
                var num =params.data.symbolSize-30
                num = num.toString()
                return 'ID：'+params.data.name + '<br />' + '姓名：'
                  + params.data.properties.phonenum + '  通讯次数：'
                  + num
              } else {
                return params.data.sourcename + ' > ' + params.data.targetname
              }

            }
          },
          //legend: [{
          //  data: categories.map(function (a) {
          //    return a.name;
          //  })
          //}],
//          label: {
//                    position: 'right',
//                    formatter: '{b}',
////                    formatter: function (params) {
////                        console.log('params', params);
////                        return
////                    }
//                },

          animationDuration: 1500,
          animationEasingUpdate: 'quinticInOut',
          //hoverLayerThreshold:99,
          series: [
            {
              name: 'Les Miserables',
              type: 'graph',
              layout: 'none',
              data: $scope.nodes.vertexes,
              links: $scope.nodes.edges,
              categories: categories,
              edgeSymbol: ['none', 'arrow'],
              roam: true,
              focusNodeAdjacency: true,
              itemStyle: {
                normal: {
                  borderColor: '#fff',
                  borderWidth: 1,
                  shadowBlur: 10,
                  shadowColor: 'rgba(0, 0, 0, 0.3)'
                }
              },
              lineStyle: {
                color: 'source',
                curveness: 0.1
              },
              emphasis: {
                lineStyle: {
                  width: 10
                }
              }
            }
          ]
        };
          //console.log('option', option);
          myChart.setOption(option);
          if (option && typeof option === "object") {
            myChart.setOption(option, true);
          }
        });


        myChart.on('click', function (params) {
          //console.log('params', params);
          var clicked = false;
          angular.forEach(clickid, function (click) {
            if (click === params.data.vid) {
              clicked = true
            }
          });
          if (clicked) {
            return
          }
          realQuery.get({
            scope:$scope.curModelScope,
            interface:'nextStep',
            label:$scope.lable,
            vid:params.data.vid,
          }, function (nodes) {
            //console.log('nodes', nodes.edges.length);
            if (nodes.edges.length === 0) {
              return
            }
            clickid.push(params.data.vid)

            //if (params.data.vid === 4232) {


              creatimage(nodes)
              $scope.nodes.vertexes = $scope.nodes.vertexes.concat(nodes.vertexes)
              $scope.nodes.vertexes = unique($scope.nodes.vertexes)
              $scope.nodes.edges = $scope.nodes.edges.concat(nodes.edges)
              rewritesize($scope.nodes.vertexes)

              option.series[0].data = $scope.nodes.vertexes
              option.series[0].links = $scope.nodes.edges
              myChart.setOption(option, {
                notMerge: false,
                lazyUpdate: false,
                silent: false
              });
            //}
          })

        });
      }
    }
  }])
