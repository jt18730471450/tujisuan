'use strict';
/**
 * Created by sorcerer on 2017/6/1.
 */
angular.module('basic.router', ['ui.router'])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/console/real-time-query");
    $stateProvider
      .state('console', {
        url: '/console',
        templateUrl: 'views/console.html',
        controller: 'ConsoleCtrl',
        abstract: true,
      })
      .state('console.real-time-query', {
        url: '/real-time-query',
        templateUrl: 'views/real-time-query.html',
        controller: 'RealTimeQueryCtrl',
        resolve: {
        //  realAllData: ['realAll',
        //    function (realAll) {
        //      return realAll.get({}).$promise
        //}
        //  ]
        }
      })
      .state('console.offline-query', {
        url: '/offline-query',
        templateUrl: 'views/offline-query.html',
        controller: 'OfflineQueryCtrl',
        resolve: {
          allTask: ['Tasklist',
            function (Tasklist) {
              return Tasklist.query({}).$promise
            }
          ]
        }
      })
      .state('console.create-offline-query', {
      url: '/create-offline/:task_id',
      params: {
        task_id: null
      },
      templateUrl: 'views/create-offline-query.html',
      controller: 'CreateOfflineQueryCtrl'
      })
      .state('console.offline-detail', {
        url: '/offline-detail/:task_id',
        templateUrl: 'views/offline-detail.html',
        controller: 'OfflineDetailCtrl',
        resolve: {
          TaskDatail: ['taskDatail', '$stateParams',
            function (taskDatail, $stateParams) {
              return taskDatail.get({task_id: $stateParams.task_id}).$promise
            }
          ]
        }
      })
  }]);
