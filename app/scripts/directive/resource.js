'use strict';

angular.module('basic.resource', ['ngResource'])
  .factory('role', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    return $resource(GLOBAL.host + '/role', {}, {});
  }])
  .factory('alltask', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    return $resource(GLOBAL.host + '/task/list', {}, {});
  }])
  // 离线查询列表 -1所有数据
  .factory('Tasklist', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    return $resource(GLOBAL.host + '/task/list?type=-1', {}, {});
  }])
  // 添加数据
  .factory('TaskAdd', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    return $resource(GLOBAL.host + '/task/add', {}, {
      put: { method: 'PUT' },
    });
  }])
  // 列表详情
  .factory('taskDatail', ['$resource', 'GLOBAL', '$stateParams', function ($resource, GLOBAL, $stateParams) {
    return $resource(GLOBAL.host + '/task/detail/:task_id', { task_id: '@task_id' }, {
      get: { method: "GET" }
    });
  }])
  // 删除数据
  .factory('deleteTaskDatail', ['$resource', 'GLOBAL', '$stateParams', function ($resource, GLOBAL, $stateParams) {
    return $resource(GLOBAL.host + '/task/delete/:task_id', { task_id: '@task_id' }, {
      delete: { method: "DELETE" }
    });
  }])
  // 停止数据
  .factory('stopTaskDatail', ['$resource', 'GLOBAL', '$stateParams', function ($resource, GLOBAL, $stateParams) {
    return $resource(GLOBAL.host + '/task/stop/:task_id', { task_id: '@task_id' }, {
      stop: { method: "PUT" }
    });
  }])
  .factory('realQuery', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    var realQuery = $resource(GLOBAL.host + '/query/:scope', {scope:'@scope'}, {});
    return realQuery;
  }])
  .factory('tasklog', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    var tasklog = $resource(GLOBAL.host + '/task/log/:id', {id:'@id'}, {});
    return tasklog;
  }])
  .factory('tasklog', ['$resource', 'GLOBAL', function ($resource, GLOBAL) {
    var tasklog = $resource(GLOBAL.host + '/task/log/:id', {id:'@id'}, {});
    return tasklog;
  }])


