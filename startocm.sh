#!/bin/sh

docker run -p 9090:9090 --name sso -d \
--env SSO_LOGIN_BASE_URL="http://10.1.235.171:12005/dmc/dev/module/login/login.html?goto=" \
--env SSO_REDEEM_BASE_URL="http://10.1.235.171:12005/dmc/ssoAuth?token=" \
--env SSO_UPSTREAM_URL="192.168.1.117:9000" \
--env SSO_REDIRECT_URI="/#/home/signin" \
--env SSO_PROXY_PREFIX="/sso/abc" \
registry.new.dataos.io/sso/sso-proxy:latest


