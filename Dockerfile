FROM registry.new.dataos.io/ocm/ocm-base:latest

COPY . /data/mainline/

WORKDIR /data/mainline

# Install nginx & node

#RUN bower install && gulp build

RUN bower install
ENV ADAPTER_API_SERVER=localhost SVCAMOUNT_API_SERVER=localhost RELEASE_EDITION='prod' SSO_SWITCH='false'

EXPOSE 9000

#ENTRYPOINT ["nginx", "-g", "daemon off;"]
#CMD ["gulp", "start:server"]
CMD ["./start.sh"]



